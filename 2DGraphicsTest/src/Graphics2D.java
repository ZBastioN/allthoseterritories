import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class Graphics2D extends JPanel implements MouseListener{


    private Polygon polygon;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


         String africa = "623 392 624 392 626 396 628 398 630 400 632 401 635 398 635 398 636 397 636 396 637 396 637 395 637 395" +
                " 637 395 639 395 640 396 642 396 643 397 643 397 643 397 644 401 646 404 647 408 648 412 648 412 648 412 649 " +
                "412 651 413 652 413 654 414 654 414 654 414 658 415 662 415 665 416 669 417 669 417 671 419 672 421 674 422 675 " +
                "423 677 423 680 422 680 422 680 422 680 421 680 421 680 420 680 420 675 418 674 415 673 413 673 410 674 407 674 403 " +
                "675 403 677 402 678 402 679 401 681 401 682 401 683 401 684 401 685 402 685 402 686 403 687 403 691 406 692 410 692 414 " +
                "693 419 692 423 690 427 691 427 692 428 693 428 694 429 695 430 695 430 696 432 697 434 697 435 698 437 698 438 700 437 703 " +
                "435 704 433 703 430 702 428 700 425 700 423 699 422 699 421 699 420 699 418 699 417 699 416 706 416 708 417 711 416 713" +
                " 416 716 415 723 412 722 418 723 421 723 424 724 427 724 430 722 433 720 436 718 438 716 439 713 441 711 441 709 443 708 444 " +
                "706 446 704 449 701 451 699 454 699 455 697 458 698 460 699 462 700 464 701 467 700 470 699 473 697 475 695 476 693 477 690 478" +
                " 689 480 688 482 688 483 688 485 688 487 689 488 689 490 684 494 680 499 676 505 671 510 666 515 659 518 656 519 655 518 653 518 " +
                "651 518 650 517 647 518 645 518 642 519 640 519 638 520 635 520 633 518 631 517 631 515 630 513 630 511 630 510 630 508 630 507" +
                " 629 506 628 504 627 503 626 501 626 500 625 498 623 496 622 493 620 491 619 489 618 487 617 484 617 481 617 476 616 472 615 467" +
                " 613 461 611 457 609 454 607 450 606 446 604 442 604 438 604 434 606 429 608 425 610 420 612 417 612 415 612 415 611 411 611 407" +
                " 610 403 610 399 610 399 609 397 609 397 608 396 608 395 608 394 607 391 608 391 612 391 615 391 618 391 622 391";

         String congo = "616 336 616 333 618 331 620 331 622 330 625 329 627 328 627 328 631 326 634 324 638 322 641 320 641 320 642 319 " +
                "644 317 645 316 647 315 648 315 650 315 651 315 652 317 653 318 653 320 654 322 654 323 655 324 658 327 661 330 664 333 667 336" +
                " 668 337 668 341 670 343 672 343 674 344 676 344 677 344 679 344 680 345 681 345 682 346 683 347 684 347 687 348 691 346 693 " +
                "346 696 346 698 347 699 352 700 357 697 361 694 363 690 366 686 368 684 370 683 372 682 373 682 375 682 376 682 377 681 379" +
                " 681 380 680 381 679 382 678 384 677 385 677 386 676 388 677 391 678 393 680 396 681 398 682 400 682 400 680 401 678 402 675 " +
                "402 673 403 673 403 674 407 673 410 673 412 673 415 674 417 679 420 679 420 679 421 679 421 679 422 679 422 679 422 675 422 674" +
                " 422 673 421 672 420 671 419 668 417 665 416 662 415 658 414 655 413 651 413 648 413 647 409 647 404 646 400 645 397 643 394 639" +
                " 395 637 395 636 397 634 398 633 399 631 400 629 399 628 398 627 397 626 395 625 394 625 393 624 392 622 390 619 390 615 390 612" +
                " 390 608 391 606 391 605 385 602 381 600 379 597 376 595 374 594 370 593 368 594 365 594 362 595 359 596 356 596 354 596 354 602" +
                " 354 607 354 613 354 618 355 618 355 618 355 619 354 621 354 622 353 623 353 623 353 622 351 620 348 618 345 616 342 615 338";


        String[] result = africa.split(" ");

        int[] xPoints = new int[ result.length / 2 ];
        int[] yPoints = new int[ result.length / 2 ];

        int count = 0;
        for ( int i = 0; i < result.length; i += 2 )
        {
            xPoints[ count ] = Integer.parseInt(result[ i ]);
            yPoints[ count ] = Integer.parseInt(result[ i + 1 ]);
            count++;
        }

        //############################################### congo map
        String[] congoArray = congo.split(" ");

        int[] congoXpoints = new int[ congoArray.length / 2 ];
        int[] congoYpoints = new int[ congoArray.length / 2 ];

        int count2 = 0;
        for ( int i = 0; i < congoArray.length; i += 2 )
        {
            congoXpoints[ count2 ] = Integer.parseInt(congoArray[ i ]);
            congoYpoints[ count2 ] = Integer.parseInt(congoArray[ i + 1 ]);
            count2++;
        }

        polygon = new Polygon( xPoints, yPoints, 150 );
        g.fillPolygon( polygon );
        g.setColor(Color.red);
        g.drawPolygon( xPoints, yPoints, 150 );
        g.setColor(Color.blue);
        g.fillPolygon( congoXpoints, congoYpoints, 150 );

        this.setBackground(Color.gray);

    }


    @Override
    public void mouseClicked(MouseEvent e) {
        if ( polygon.contains(getX(), getY()) )
        {
            // TODO how to add color changing possibility
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
