import java.awt.*;

public class Patch {

    private String territoryName;
    private int[] xCoords;
    private int[] yCoords;

    Patch( String territoryName, int[] xCoords, int[] yCoords )
    {
        this.territoryName = territoryName;
        this.xCoords = xCoords;
        this.yCoords = yCoords;
    }


    public Polygon toPoly()
    {
        Polygon poly = new Polygon ( xCoords, yCoords, xCoords.length );
        return poly;
    }


    // ############## GETTER AND SETTER #################

}
