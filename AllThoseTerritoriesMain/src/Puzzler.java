import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import java.awt.*;
import java.util.Deque;
import java.util.LinkedList;

public class Puzzler {

    private Deque<Territory> territoryList = new LinkedList<>();
    private Deque<Continent> continentList = new LinkedList<>();

    // ALL COLORS FOR DRAWING HERE
    private Color c_neutral = new Color(0x999999);
    private Color c_neutral_light = new Color(0xBBBBBB);
    private Color c_neutral_border = new Color(0x000000);

    private Color c_player = new Color(0x3355FF);
    private Color c_player_light = new Color(0x5577FF);
    private Color c_player_border = new Color(0x1133DD);

    private Color c_npc = new Color(0xCC2222);
    private Color c_npc_light = new Color(0xFF4444);
    private Color c_npc_border = new Color(0x992222);

    Puzzler ()
    {
    }

    Puzzler ( String mapData )
    {
        String[] territories = mapData.split("\n");

        for ( int i = 0; i < territories.length; i++ ) // split every line
        {
            if ( !territories[i].isEmpty() ) decode ( territories[i] );
        }

        /*for ( Territory t : territoryList ) {
            System.out.println(t.toString());
        }*/
    }



    // DECODE MAIN SPLITTER
    private void decode ( String tData )
    {
        String[] inf = tData.split(" ");
        int select;

        if( inf[0].equals("patch-of") )
            select = 0;
        else if ( inf[0].equals("capital-of") )
            select = 1;
        else if ( inf[0].equals("neighbors-of") )
            select = 2;
        else if ( inf[0].equals("continent") )
            select = 3;
        else
        {
            JOptionPane.showMessageDialog( null, "Corrupt Map File!", "Decode error", JOptionPane.ERROR_MESSAGE );
            select = -1;
        }

        if ( select != -1 )
        {
            if ( select == 0 )
                decode_patch ( inf );
            else if ( select == 1 )
                decode_cap ( inf );
            else if ( select == 2 )
            {
                decode_neighbors ( inf );
            }
            else
            {
                decode_continent( inf );
            }
        }
    }

    // CREATE TERRITORY
    private void decode_patch (String[] tData)
    {
        String nameAndOffset = getName( tData );
        String[] nsSplit = nameAndOffset.split(":");

        int offset = 1;
        String territoryName = nsSplit[1];

        try {
            offset = Integer.parseInt(nsSplit[0]);
        }
        catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "Error while decoding the PATCH: " + nsSplit[1], "Decode error", JOptionPane.ERROR_MESSAGE );}

        String[] pointData = getCoords( tData, offset );

        int[] xPoints = new int[ pointData.length / 2 ];
        int[] yPoints = new int[ pointData.length / 2 ];

        for ( int i = 0; i < pointData.length; i += 2 )
        {
            try {
                xPoints[i / 2] = Integer.parseInt(pointData[i]);
                yPoints[i / 2] = Integer.parseInt(pointData[i + 1]);
            } catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "PATCH coordinates failed to decode correctly", "Decode error", JOptionPane.ERROR_MESSAGE );}
        }

        Patch patch = new Patch( territoryName, xPoints, yPoints );
        boolean matched = false;


        for ( Territory t : territoryList )
        {
            if ( t.getName().equals(territoryName) ) // checks for a territory with the same name
            {
                t.addPatch(patch);
                matched = true;
            }
        }

        if ( !matched )
        {
            territoryList.add( new Territory(territoryName) );

            for ( Territory t : territoryList )
            {
                if ( t.getName().equals(territoryName) ) // checks if names are equal
                {
                    t.addPatch(patch);
                }
            }
        }

    }

    // ADD CAPITAL COORDINATES TO RELATED PATCHES
    private void decode_cap (String[] cData)
    {
        String nameString = getName( cData );
        String[] nsSplit = nameString.split(":");

        int offset = 1;
        String name = nsSplit[1];

        try {
            offset = Integer.parseInt(nsSplit[0]);
        }
        catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "Error while decoding the CAPITAL: " + nsSplit[1], "Decode error", JOptionPane.ERROR_MESSAGE );}

        String[] pointData = getCoords( cData, offset );

        for ( Territory t : territoryList )
        {
            if( name.equals( t.getName() ) )
            {
                try {
                    t.setxCap( Integer.parseInt(pointData[0]) );
                    t.setyCap( Integer.parseInt(pointData[1]) );
                } catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "CAPITAL coordinates failed to decode correctly", "Decode error", JOptionPane.ERROR_MESSAGE );}
            }
        }
    }

    // ADD NEIGHBORS TO RELATED PATCHES
    private void decode_neighbors (String[] nData)
    {
        String nameString = getName( nData );
        String[] nsSplit = nameString.split(":");

        int offset = 1;
        String name = nsSplit[1];

        try {
            offset = Integer.parseInt(nsSplit[0]);
        }
        catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "Error while decoding the NEIGHBORS of: " + nsSplit[1], "Decode error", JOptionPane.ERROR_MESSAGE );}

        String[] neighbors = getTerritories( nData, offset );

        for ( Territory base : territoryList )
        {
            if( name.equals( base.getName() ) )
            {
                String[] base_old_neighbors = base.getNeighbors();
                if ( base_old_neighbors != null )
                {
                    String[] base_new_neighbors = new String[ base_old_neighbors.length + neighbors.length ];
                    for ( int j = 0; j < base_old_neighbors.length; j++)
                    {
                        base_new_neighbors[j] = base_old_neighbors[j];
                    }
                    for ( int j = base_old_neighbors.length; j < base_old_neighbors.length + neighbors.length; j++)
                    {
                        base_new_neighbors[j] = neighbors[ j - base_old_neighbors.length ];
                    }
                    base.setNeighbors(base_new_neighbors);
                }
                else
                {
                    base.setNeighbors(neighbors);
                }

                for ( Territory neighbor : territoryList )
                {
                    for ( int i = 0; i < neighbors.length; i++ )
                    {
                        if ( neighbor.getName().equals( neighbors[ i ] ) )
                        {
                            String[] old_neighbors = neighbor.getNeighbors();
                            if ( old_neighbors != null )
                            {
                                String[] new_neighbors = new String[ old_neighbors.length + 1 ];
                                for ( int j = 0; j < old_neighbors.length; j++)
                                {
                                    new_neighbors[j] = old_neighbors[j];
                                }
                                new_neighbors[old_neighbors.length] = name;
                                neighbor.setNeighbors(new_neighbors);
                            }
                            else
                            {
                                String[] newNeighbors = new String[1];
                                newNeighbors[0] = name;
                                neighbor.setNeighbors(newNeighbors);
                            }
                        }
                    }
                }
            }
        }
    }

    // CREATE CONTINENTS
    private void decode_continent (String[] cData)
    {
        String nameString = getName( cData );
        String[] nsSplit = nameString.split(":");

        int offset = 1;
        int reinforcements = 0;
        String name = nsSplit[1];

        try {
            offset = Integer.parseInt(nsSplit[0]);
            reinforcements = Integer.parseInt( cData[offset] );
        }
        catch (NumberFormatException e) {JOptionPane.showMessageDialog( null, "Error while decoding the CONTINENT: " + nsSplit[1], "Decode error", JOptionPane.ERROR_MESSAGE );}

        String[] patches = getTerritories( cData, offset + 1 ); // add one to offset to circumvent reinforcements entry

        continentList.add( new Continent(name, patches, reinforcements) );
    }

    // ########################## TERRITORY DRAW FUNCTION ##########################

    public void draw ( Graphics graphics )
    {
        for ( Territory t : territoryList ) {

            // DRAW CONNECTIONS
            String[] neighbors = t.getNeighbors();

            for ( int i = 0; i < neighbors.length; i++ ) {
                for ( Territory n : territoryList ) {
                    if ( n.getName().equals( neighbors[i] ) ) {
                        graphics.drawLine( t.getxCap(), t.getyCap(), n.getxCap(), n.getyCap() );
                        break;
                    }
                }
            }

            graphics.setColor( t.getColor() );
            for ( Polygon p : t.toPolygons() )
            {
                graphics.fillPolygon( p );
            }

            graphics.setColor( t.getBorderColor() );
            for ( Polygon p : t.toPolygons() )
            {
                graphics.drawPolygon( p );
            }



            // RELATED TROOPS TO PATCH
            graphics.drawString( "" + t.getTroops(), t.getxCap() - 3, t.getyCap() + 5 );
        }
    }

    // ########################## PRIVATE HELP FUNCTIONS ##########################

    public boolean totalDomination ()
    {
        int owner = territoryList.peek().getOwned();
        for ( Territory t : territoryList )
        {
            if ( t.getOwned() != owner )
                return false;
        }
        return true;
    }

    private static boolean isNumeric ( String str )
    {
        try
        {
            double d = Double.parseDouble ( str );
        }
        catch( NumberFormatException nfe )
        {
            return false;
        }
        return true;
    }

    private static String[] getCoords ( String[] dString, int offset )
    {
        String[] pointData = new String[ dString.length - offset ];

        for ( int i = offset; i < dString.length; i++ )
        {
            pointData[ i - offset ] = dString[ i ];
        }
        return pointData;
    }



    private static String[] getTerritories ( String[] dString, int offset )
    {
        int patch_counter = 1;
        for ( int i = 0; i < dString.length; i++ )
        {
            if ( dString[i].equals("-") )
                patch_counter++;
        }

        String[] territoryData = new String[ patch_counter ];

        int entry_counter = 0;
        for ( int i = offset + 1; i < dString.length; i++ )
        {
            if ( !dString[i].equals("-") )
            {
                if ( territoryData[entry_counter] == null )
                    territoryData[ entry_counter ] = dString[i];
                else
                    territoryData[ entry_counter ] += " " + dString[i];
            }
            else
            {
                entry_counter++;
            }
        }
        return territoryData;
    }


    // returns the name & offset
    private static String getName ( String[] dString )
    {
        String name = "";
        int offset = 1;

        for ( int i = offset; i < dString.length; i++ ) // iterate through array until all name parts are specified
        {
            if ( !isNumeric( dString[i] ) && !dString[i].equals(":") ) // number-check for patches, caps and continents, colon for neighbors
            {
                if ( name != "" )
                {
                    name += " ";
                }
                name += dString[i];
                offset++;
            }
            else
            {
                break;
            }

        }

        return offset + ":" + name; // return coded as [offset]:[full name] since the offset is related to the name and of importance later on
    }

    // ########################## GETTERS AND SETTERS ##########################


    public Color getC_player_light() {
        return c_player_light;
    }

    public Color getC_npc_light() {
        return c_npc_light;
    }

    public Color getC_neutral_light() {
        return c_neutral_light;
    }

    public Color getC_neutral_border() {
        return c_neutral_border;
    }

    public Color getC_player_border() {
        return c_player_border;
    }

    public Color getC_npc_border() {
        return c_npc_border;
    }

    public Color getC_neutral() {
        return c_neutral;
    }

    public Color getC_player() {
        return c_player;
    }

    public Color getC_npc() {
        return c_npc;
    }

    public Deque<Territory> getTerritoryList() {
        return territoryList;
    }

    public Deque<Continent> getContinentList() {
        return continentList;
    }
}
