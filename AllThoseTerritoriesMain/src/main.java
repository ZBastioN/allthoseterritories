import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;

public class main extends JPanel implements ActionListener{


    // ########################## BOARD CLASS START #######################

    public class gameBoard extends JPanel implements MouseListener, MouseMotionListener
    {
        // Game Vars
        private int g_phase = 0; // 0 .. Deployment, 1 .. Conquer
        private boolean g_turn = true; // true = player, false = cpu
        private int leftTerritoriesToChoose;
        private boolean firstRun = true;

        private int cpuTerritoriesCount = 0;
        private int cpuTroopsToSet = 0;
        private int cpuDistributedTroops = 0;
        private int playerTerritoriesCount = 0;
        private int playerTroopsToSet = 0;
        private int playerDistributedTroops = 0;

        private Deque<Territory> list;

        gameBoard()
        {

        }

        protected void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            this.setBackground(c_game_background);

            if ( puzzler!= null )
            {
                list = puzzler.getTerritoryList();

                if ( puzzler.totalDomination() && list.peek().getOwned() != -1 )
                {
                    if (list.peek().getOwned() == 0)
                        JOptionPane.showMessageDialog( null, "Good job, now get yourself a cookie..", "YOU WON!", JOptionPane.YES_OPTION );
                    else
                        JOptionPane.showMessageDialog( null, "That's a bit sad, so... against a stupid AI right? No? Alright, maybe next time.", "YOU LOSE!", JOptionPane.YES_OPTION );
                }

                if ( firstRun ) {
                    addMouseListener(this);
                    addMouseMotionListener(this);
                    leftTerritoriesToChoose = getTerritoryCount();
                    firstRun = false;
                }

                if ( g_phase == 0 ) // PHASE 0
                {
                    if ( g_turn )
                    {
                        statusPanel.setText("Your turn");
                    }
                    else
                    {
                        statusPanel.setText("Computers turn");
                        cpu_choose();
                    }
                    if ( leftTerritoriesToChoose == 0)
                    {
                        g_phase = 1;
                        g_turn = true;
                    }
                }
                else if ( g_phase == 1 ) // PHASE 1
                {
                    int cpuBoni = calcBoni( 1 );
                    int playerBoni = calcBoni ( 0 );
                    statusPanel.setText("Verstärkungen verteilen");
                    cpuTerritoriesCount = getOwnCount(1);
                    cpuTroopsToSet = cpuTerritoriesCount / 3 + cpuBoni;
                    playerTerritoriesCount = getOwnCount(0);
                    playerTroopsToSet = playerTerritoriesCount / 3 + playerBoni;
                    statusPanelTop.setText(" CPU Territories: " + cpuTerritoriesCount + "   Player Territories: " + playerTerritoriesCount + " ;   CPU Reinforcements: "
                            + cpuTroopsToSet + "   Player Reinforcements: " + playerTroopsToSet);
                    if ( !g_turn ) cpu_reinforcment();
                }
                else if ( g_phase == 2 ) // PHASE 2
                {
                    // RESET LAST PHASE COUNTERS START
                    cpuDistributedTroops = 0;
                    playerDistributedTroops = 0;
                    // RESET LAST PHASE COUNTERS END


                    statusPanel.setText("Angreifen und bewegen");

                    String topText = " turn to attack!";
                    if ( g_turn )
                        statusPanelTop.setText( "Players" + topText );
                    else
                        statusPanelTop.setText( "CPUs" + topText );


                }
                else if ( g_phase == 3 ) // PHASE 3
                {
                    statusPanel.setText("Bewegen");

                    String topText = " turn to move!";
                    if ( g_turn )
                        statusPanelTop.setText( "Players" + topText );
                    else
                        statusPanelTop.setText( "CPUs" + topText );
                }
                else if ( g_phase == 4 ) // CPU ATTACK
                {
                    cpu_attack();
                    g_phase = 1;
                    g_turn = true;
                }


                puzzler.draw(g);
            }
        }

        private void g_attacks( Territory from, Territory to )
        {
            int attackers = from.getTroops();
            int defenders = to.getTroops();
            boolean noFight = false;

            if (attackers < 2)
            {
                from.setSelected( false );
                statusPanelTop.setText( "Es muss zumindest eine Einheit im Urpsrungsland zurückbleiben!" );
                noFight = true;
            }
            if (defenders == 0)
            {
                statusPanelTop.setText( "Sie haben " + to.getName() + " eingenommen!" );
                to.setOwned( 0 );
                to.setTroops( from.getTroops() - 1 );
                from.setTroops( 1 );
                noFight = true;
            }

            if( !noFight )
            {
                while ( attackers > 1 && defenders > 0 )
                {
                    int[] vals = g_rollDice(attackers, defenders);
                    attackers = vals[0];
                    defenders = vals[1];
                }

                if ( defenders == 0 )
                {
                    statusPanelTop.setText( "Sie haben " + to.getName() + " eingenommen!" );
                    to.setOwned( from.getOwned() );
                    to.setTroops( from.getTroops() - 1 );
                    from.setTroops( 1 );
                }
                else if ( attackers == 1 )
                {
                    statusPanelTop.setText( "Sie haben den Kampf um " + to.getName() + " verloren!" );
                    from.setTroops( 1 );
                    to.setTroops( defenders );
                }
            }
        }

        private void cpu_choose()
        {
            if ( leftTerritoriesToChoose > 0 ) {
                for (Territory t : list) {
                    if (t.getOwned() == -1) {
                        t.setOwned(1);
                        t.setTroops(1);
                        g_turn = true;
                        leftTerritoriesToChoose -= 1;
                        break;
                    }
                }
            }
        }

        private void cpu_reinforcment()
        {
            if (cpuTroopsToSet != 0)
            {
                for (Territory t : list) {
                    if (t.getOwned() == 1 && cpuDistributedTroops < cpuTroopsToSet) {
                        for ( Territory enemy : list )
                        {
                            if ( enemy.getOwned() == 0 && enemy.checkNeighbor(t) && cpuDistributedTroops <= cpuTroopsToSet )
                            {
                                t.setTroops(t.getTroops() + cpuTroopsToSet);
                                cpuDistributedTroops = cpuTroopsToSet;
                                g_phase = 2;
                                g_turn = true;
                                menuItem_endAttacking.setEnabled(true);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                g_phase = 2;
                g_turn = true;
                menuItem_endAttacking.setEnabled(true);
            }
        }

        private void cpu_attack()
        {
            for (Territory t : list) {
                if ( t.getOwned() == 1 )
                {
                    for ( Territory t2 : list )
                    {
                        if ( t2.getOwned() == 0 && t.checkNeighbor( t2 ) )
                        {
                            if ( t.getTroops() > t2.getTroops() )
                            {
                                g_attacks( t, t2 );
                            }
                        }
                    }
                }
            }
        }


        private int getTerritoryCount()
        {
            int result = 0;
            for ( Territory t : list )
            {
                result++;
            }
            return result;
        }

        private int getOwnCount( int ownedBy )
        {
            int result = 0;
            for ( Territory t : list )
            {
                if ( t.getOwned( ) == ownedBy ) result++;
            }
            return result;
        }

        private int[] sortRolls( int[] arr )
        {
            int temp;
            for( int i=1; i < arr.length; i++ )
            {
                for( int j=0; j < arr.length-i; j++ )
                {
                    if( arr[j] > arr[j+1] )
                    {
                        temp = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = temp;
                    }

                }
            }

            return arr;
        }

        private int calcBoni ( int player )
        {
            int ret = 0;

            Deque<Continent> continents = puzzler.getContinentList();

            for ( Continent c : continents )
            {
                if ( c.isDominated( list, player ) )
                    ret += c.getReinforcements();
            }


            return ret;
        }

        private int[] g_rollDice( int atts, int defs )
        {
            int[] attRolls = new int[2];
            int[] defRolls = new int[1];
            if ( atts >= 4 )
                attRolls = new int[3];
            else if ( atts == 2 )
                attRolls = new int[1];

            if ( defs >= 3 )
                defRolls = new int[2];

            Random rand = new Random();
            for ( int i = 0; i < defRolls.length; i++ )
            {
                defRolls[i] =  1 + rand.nextInt(6);
            }
            for ( int i = 0; i < attRolls.length; i++ )
            {
                attRolls[i] = 1 + rand.nextInt(6);
            }

            defRolls = sortRolls( defRolls );
            attRolls = sortRolls( attRolls );

            int greatest = attRolls.length;
            if ( defRolls.length < attRolls.length )
                greatest = defRolls.length;

            for ( int i = 0; i < greatest; i++ )
            {
                if ( attRolls[i] > defRolls[i] )
                {
                    defs--;
                }
                else
                {
                    atts--;
                }
            }

            return new int[]{atts, defs};
        }

        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

            if (g_phase == 0) // DEPLOYMENT
            {
                for (Territory t : list) {
                    for (Patch p : t.getPatchList()) {
                        if (p.toPoly().contains(e.getX(), e.getY()) && t.getOwned() == -1) {
                            if (g_turn) // PLAYER
                            {
                                t.setOwned(0);
                                t.setTroops(1);
                                g_turn = false;
                                leftTerritoriesToChoose -= 1;
                            }
                        }
                    }
                }
            }
            else if ( g_phase == 1 ) // REINFORCEMENT
            {
                for (Territory t : list) {
                    for (Patch p : t.getPatchList()) {
                        if ( p.toPoly().contains(e.getX(), e.getY()) && t.getOwned() == 0) {
                            if ( g_turn ) // PLAYER
                            {
                                if ( playerDistributedTroops < playerTroopsToSet )
                                {
                                    t.setTroops( t.getTroops() + 1 );
                                    playerDistributedTroops += 1;
                                }
                                if ( playerDistributedTroops >= playerTroopsToSet )
                                {
                                    g_turn = false;
                                }
                            }
                        }
                    }
                }
            }
            else if ( g_phase == 2 ) // ATTACK
            {
                for (Territory t : list) {
                    for (Patch p : t.getPatchList()) {
                        if ( p.toPoly().contains(e.getX(), e.getY() ) ) {
                            if ( g_turn ) // PLAYER
                            {
                                if ( t.getOwned() == 0 && !t.isSelected() )
                                {
                                    t.setSelected( true );
                                    t.setColor( puzzler.getC_player_light() );
                                    for (Territory others : list) {
                                        if ( !others.getName().equals( t.getName() ) && others.getOwned() == 0 )
                                        {
                                            others.setSelected( false );
                                            others.setColor( puzzler.getC_player() );
                                        }
                                    }
                                }
                                else if ( t.getOwned() == 0 && t.isSelected() )
                                {
                                    t.setSelected( false );
                                }
                                else if ( t.getOwned() == 1 )
                                {
                                    for (Territory base : list) {
                                        if ( base.getOwned() == 0 && base.isSelected() && base.checkNeighbor( t ) )
                                        {
                                            g_attacks ( base, t );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if ( g_phase == 3 ) // TRUPPEN VERTEILEN
            {
                for (Territory t : list) {
                    for (Patch p : t.getPatchList()) {
                        if ( p.toPoly().contains(e.getX(), e.getY()) && t.getOwned() == 0 ) {
                            if (!SwingUtilities.isRightMouseButton(e))
                            {
                                if (!t.isSelected()) {
                                    t.setSelected(true);
                                    t.setColor(puzzler.getC_player_light());
                                    for (Territory others : list) {
                                        if ( !others.getName().equals( t.getName() ) && others.getOwned() == 0 )
                                        {
                                            others.setSelected( false );
                                            others.setColor( puzzler.getC_player() );
                                        }
                                    }
                                } else if (t.isSelected()) {
                                    t.setSelected(false);
                                }
                            }
                            if ( SwingUtilities.isRightMouseButton( e ) || e.isControlDown() )
                            {
                                for (Territory others : list) {
                                    if ( others.isSelected() && others.getOwned() == 0 )
                                    {
                                        String[] neighbors = others.getNeighbors();
                                        for ( String n :  neighbors )
                                        {
                                            if ( n.equals( t.getName() ) )
                                            {
                                                if ( others.getTroops() > 1 )
                                                {
                                                    t.setTroops(t.getTroops() + 1);
                                                    others.setTroops(others.getTroops() - 1);
                                                    others.setSelected(false);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

        @Override
        public void mouseMoved(MouseEvent e) {
            for( Territory t : list )
            {
                for ( Patch p : t.getPatchList() )
                {
                    if ( p.toPoly().contains( e.getX(), e.getY() ) && !t.isSelected() )
                    {

                        Color c;
                        if ( t.getOwned() == 0 )
                            c = puzzler.getC_player_light();
                        else if ( t.getOwned() == 1 )
                            c = puzzler.getC_npc_light();
                        else
                            c = puzzler.getC_neutral_light();

                        t.setColor( c );
                    }
                    else if ( !p.toPoly().contains( e.getX(), e.getY() ) && !t.isSelected() )
                    {
                        Color c;
                        if ( t.getOwned() == 0 )
                            c = puzzler.getC_player();
                        else if ( t.getOwned() == 1 )
                            c = puzzler.getC_npc();
                        else
                            c = puzzler.getC_neutral();

                        t.setColor( c );
                    }
                }
            }
            repaint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {

        }

        public int getG_phase() {
            return g_phase;
        }

        public void setG_phase(int g_phase) {
            this.g_phase = g_phase;
        }

        public boolean isFirstRun() {
            return firstRun;
        }

        public void setFirstRun(boolean firstRun) {
            this.firstRun = firstRun;
        }

        public void setG_turn( boolean g_turn )
        {
            this.g_turn = g_turn;
        }
    }


    // ########################## BOARD CLASS END ##########################



    // FileChooser
    private final static JFileChooser mapSelect = new JFileChooser(System.getProperty("user.dir"));


    // StatusPanel
    JLabel statusPanel;
    JLabel statusPanelTop;


    // Board
    gameBoard board;

    // Main Window
    JFrame mainWindow;

    // Menue Bar
    JMenuBar menueBar;
    JMenu fileMenue;
    JMenuItem menueItem_quit;
    JMenuItem menuItem_chooseMap;
    JMenuItem menuItem_endAttacking;
    JMenuItem menuItem_endRound;

    // Puzzler
    Puzzler puzzler;


    // Colors
    public Color c_game_background = new Color(109, 206, 238);

    public static void main (String[] args)
    {
        //Typ auf Umgebungsstil aendern
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Failed to look and feel.", "No looks. No feels.", JOptionPane.ERROR_MESSAGE);
        }
        //Applet starten
        new main().init();
    }

    private void init () {
        // Standard Window Management
        mainWindow = new JFrame();
        mainWindow.setLayout(new BorderLayout());
        mainWindow.setTitle("All Those Territories");
        mainWindow.setSize(1250, 650);
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setResizable(false);


        // Menuleiste
        menueBar = new JMenuBar();
        mainWindow.setJMenuBar(menueBar);

        fileMenue = new JMenu("Settings");
        fileMenue.setMnemonic('S');
        menueBar.add(fileMenue);

        // New Map Menue
        menuItem_chooseMap = new JMenuItem("Choose Map");
        menuItem_endAttacking = new JMenuItem("End Attacks");
        menuItem_endRound = new JMenuItem("End Round");
        menuItem_chooseMap.setMnemonic('N');
        menuItem_endAttacking.setMnemonic('E');
        menuItem_endAttacking.setMnemonic('R');
        menuItem_endAttacking.setAccelerator(KeyStroke.getKeyStroke("control E"));
        menuItem_chooseMap.setAccelerator(KeyStroke.getKeyStroke("control N"));
        menuItem_endRound.setAccelerator(KeyStroke.getKeyStroke("control R"));
        menuItem_chooseMap.addActionListener(newMapListener);
        menuItem_endAttacking.addActionListener( endAttackListener );
        menuItem_endRound.addActionListener( endRoundListener );
        fileMenue.add(menuItem_chooseMap);
        fileMenue.add(menuItem_endAttacking);
        fileMenue.add(menuItem_endRound);
        menuItem_endAttacking.setEnabled(false);
        menuItem_endRound.setEnabled(false);
        fileMenue.addSeparator();


        // Quit
        menueItem_quit = new JMenuItem("Quit");
        menueItem_quit.setMnemonic('Q');
        menueItem_quit.setAccelerator( KeyStroke.getKeyStroke("alt F4") );
        menueItem_quit.addActionListener( quitListener );
        fileMenue.add( menueItem_quit );

        // Status Panel
        statusPanel = new JLabel(" Please load a map");
        mainWindow.add( statusPanel, BorderLayout.PAGE_END );
        statusPanelTop = new JLabel(" Click 'Settings' to load a map");
        mainWindow.add( statusPanelTop, BorderLayout.PAGE_START );

        // Configure File Selector
        mapSelect.addChoosableFileFilter(new FileNameExtensionFilter("Map Files", "map"));
        mapSelect.setAcceptAllFileFilterUsed(false);

        board = new gameBoard();
        mainWindow.add( board, BorderLayout.CENTER );


        mainWindow.setVisible(true);

    }

    // READ NEW MAP
    private final ActionListener newMapListener = new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
            int approve = mapSelect.showOpenDialog(null);

            if (approve == JFileChooser.APPROVE_OPTION)
            {
                try
                {
                    FileReader fr = new FileReader (mapSelect.getSelectedFile());
                    BufferedReader br = new BufferedReader(fr);

                    String mapData = "";
                    String curr;

                    while ( (curr = br.readLine()) != null) // prints every line of the file into mapData String
                    {
                        mapData += curr + "\n";
                    }

                    statusPanel.setText(" Loaded " + mapSelect.getName(mapSelect.getSelectedFile()) + " successfully!");
                    puzzler = new Puzzler( mapData );

                    board.setG_phase( 0 );
                    board.setFirstRun( true );
                    board.setG_turn(true);
                    statusPanelTop.setText(" Loaded, now playing");

                    board.repaint();

                }
                catch (IOException ioe)
                {
                    JOptionPane.showMessageDialog(null, "We're sorry, but something went terribly wrong.", "Failed to read file", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    };

    private final ActionListener quitListener = new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
            //Quitting
            System.exit(0);
        }
    };

    private final ActionListener endAttackListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            board.setG_phase( 3 );
            menuItem_endAttacking.setEnabled(false);
            menuItem_endRound.setEnabled(true);
        }
    };

    private final ActionListener endRoundListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            board.setG_phase( 4 );
            menuItem_endRound.setEnabled(false);
        }
    };

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}