import java.awt.*;
import java.util.Deque;
import java.util.LinkedList;


public class Territory {

    private Deque<Patch> patchList = new LinkedList<>();

    private int troops = 0;
    private int xCap, yCap;

    private String name;
    private String[] neighbors;
    private int owned = -1;

    private Color color = new Color(0x999999);
    private Color borderColor = new Color(0x000000);

    private boolean selected = false;



    Territory ( String name )
    {
        this.name = name;
    }

    public Polygon[] toPolygons()
    {
        Deque<Polygon> polys = new LinkedList<>();

        for ( Patch p : patchList )
        {
            polys.add(p.toPoly());
        }

        Polygon[] ret = new Polygon[ polys.size() ];

        for ( int i = 0; i < patchList.size(); i++ )
        {
            ret[ i ] = polys.poll();
        }
        return ret;
    }

    public void addPatch ( Patch patch )
    {
        patchList.add( patch );
    }

    // ########################## GETTER AND SETTER START ##########################


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getTroops() {
        return troops;
    }

    public void setTroops(int troops) {
        this.troops = troops;
    }

    public String[] getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(String[] neighbors) {
        this.neighbors = neighbors;
    }

    public int getxCap() {
        return xCap;
    }

    public void setxCap(int xCap) {
        this.xCap = xCap;
    }

    public int getyCap() {
        return yCap;
    }

    public void setyCap(int yCap) {
        this.yCap = yCap;
    }

    public String getName() {
        return name;
    }

    public void setColor(Color color) {
        Puzzler p = new Puzzler();
        this.color = color;
        if ( this.color == p.getC_player() )
            this.setBorderColor( p.getC_player_border() );
        else if ( this.color == p.getC_npc() )
            this.setBorderColor( p.getC_npc_border() );
    }

    public Color getColor()
    {
        return this.color;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public Deque<Patch> getPatchList() {
        return patchList;
    }

    public int getOwned() {
        return owned;
    }

    public void setOwned(int owned) {
        this.owned = owned;
        Puzzler p = new Puzzler();

        if ( this.owned == 0 ) // PLAYER
        {
            this.setColor( p.getC_player() );
        }
        else if ( this.owned == 1 ) // CPU
        {
            this.setColor( p.getC_npc() );
        }
    }

    public String toString()
    {
        String ret = "";

        ret += name + " : {";
        try {
            for (int i = 0; i < neighbors.length; i++) {
                ret += neighbors[i] + ",";
            }
            ret += "}";
        } catch (NullPointerException npe){
            System.err.println("NPE ERROR: " + name);
        }

        ret += ", {";

        try {

            ret += patchList.size() + "}";
        } catch (NullPointerException npe){
            System.err.println("NPE ERROR: " + name);
        }

        return ret;
    }

    public boolean checkNeighbor ( Territory t )
    {
        for ( String n : neighbors )
        {
            if ( t.getName().equals(n) )
                return true;
        }
        return false;
    }


}
