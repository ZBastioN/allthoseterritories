import java.util.Deque;

public class Continent {

    private String name;
    private String[] patches;
    private int reinforcements;


    public Continent(String name, String[] patches, int reinforcements)
    {
        this.name = name;
        this.patches = patches;
        this.reinforcements = reinforcements;
    }

    public int getReinforcements()
    {
        return reinforcements;
    }

    public boolean isDominated (Deque<Territory> list, int o)
    {
        int owner = o;

        for ( String s : patches )
        {
            for ( Territory t : list )
            {
                if ( t.getOwned() != owner && s.equals( t.getName() ) )
                    return false;
            }
        }

        return true;
    }
}
